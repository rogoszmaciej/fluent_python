######################
# Utilities
######################
update-requirements:
	pip install -U pip pip-tools
	pip-compile -U requirements.in
	pip-compile -U requirements-test.in

black:
	python3 -m black ./
