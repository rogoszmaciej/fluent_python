import time


var_b = "B"


def func_1(var_a):
    res = var_a + var_b  # noqa F823 -> for showcase purposes
    var_b = "BB"  # noqa F841 -> for showcase purposes
    return res


def func_2(var_a):
    global var_b
    res = var_a + var_b
    var_b = "BB"
    return res


def deco(func):
    def inner():
        return "Running deco__inner"

    return inner


@deco
def target():
    return "Running target"


def make_averager():
    series = []

    def averager(new_value):
        # The closure for averager extends to include free variable series from make_averager closure.
        # NOTE: It must be mutable, otherwise exception will be thrown.
        series.append(new_value)
        return sum(series) / len(series)

    return averager


def clock(func):
    def clocked(*args):
        start = time.perf_counter()
        result = func(*args)
        elapsed = time.perf_counter() - start
        arg_str = ", ".join(repr(arg) for arg in args)
        print(f"Elapsed: {elapsed:0.8f}s")
        print(f"Function name: {func.__name__}")
        print(f"{arg_str} -> {result}")
        return result

    return clocked
