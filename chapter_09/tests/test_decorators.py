import pytest
from chapter_09.decorators import target, func_1, func_2, make_averager


def test_target_is_decorated():
    # Message is the same as in decorator, not the function itself.
    assert target() == "Running deco__inner"


def test_variable_scope():
    with pytest.raises(UnboundLocalError):
        # This will raise error as var_b inside func_1 is declared in global and local scope.
        # Thus Python will treat it as a local variable and raise proper exception.
        func_1("A")


def test_global_keyword():
    assert func_2("A") == "AB"


def test_closures():
    avg = make_averager()

    assert avg(10) == 10.0
    assert avg(12) == 11.0
    assert avg(14) == 12.0
